class updater (
  Optional[String] $serial          = $updater::params::serial,
  Array $excludes                   = $updater::params::excludes,
  String $provider                  = $updater::params::provider,
  Boolean $reboot                   = $updater::params::reboot,
  Optional[String] $reboot_schedule = $updater::params::reboot_schedule,
) inherits updater::params {

    if ( $serial != undef ) {

        if $serial !~ /^[0-9]+$/ {
            fail('Invalid value: serial (expected: YYYYMMDDNN)')
        }

        $excludes_arr = $excludes.map |$exclude| {
            "--exclude=${exclude}"
        }
        $excludes_str = join($excludes_arr, ' ')

        exec { "${provider} update":
            provider => 'shell',
            command  => "${provider} clean all && ${provider} -y update ${excludes_str} && touch /var/local/.update-${provider}-${serial}",
            creates  => "/var/local/.update-${provider}-${serial}",
            path     => '/usr/bin:/usr/sbin:/bin',
            timeout  => 7200,
        }

        if $reboot {

            if ( $reboot_schedule != undef ) {
                if $reboot_schedule =~ /^[0-9:,*~ -]+$/ { # lazy regex. example: *-*-* 12:15:00
                    exec { "reboot schedule":
                        provider => 'shell',
                        command  => "touch /var/local/.reboot-schedule-${serial} && systemd-run --unit=one-off-reboot --on-calendar='${reboot_schedule}' /usr/bin/systemctl reboot",
                        creates  => "/var/local/.reboot-schedule-${serial}",
                        path     => '/usr/bin:/usr/sbin:/bin',
                    }
                }
                else {
                    fail('Invalid value: reboot_schedule (expected: systemd.time)')
                }
            }

            else {
                reboot { "${provider} update reboot":
                    subscribe => Exec["${provider} update"],
                    apply     => finished,
                }
            }
        }

    }
}