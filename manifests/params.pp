class updater::params {
    $_defaults = {
        'provider' => 'dnf',
    }

    case $facts['os']['family'] {
        'RedHat': {
            if ($facts['os']['name'] in ['RedHat', 'CentOS', 'Rocky'] and $facts['os']['release']['major'] in ['6', '7']) {
                $_overrides = {
                'provider' => yum,
                }
            } else {
                $_overrides = {
                'provider' => 'dnf',
                }
            }
        }
        default: {
            case $facts['os']['name'] {
                default: { $_overrides = {} }
            }
        }
    }
    $_params = merge($_defaults, $_overrides)

    $serial   = undef
    $excludes = []
    $provider = $_params['provider']
    $reboot   = false
    $reboot_schedule = undef
}